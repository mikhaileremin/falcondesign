<?php

class PortfolioController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('CreateGallery','DeleteGallery','RenameGallery','UploadImage','UpdateImagesOrder',"DeleteImage","UpdateImageDescription"),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('CreateGallery','DeleteGallery','RenameGallery','UploadImage','UpdateImagesOrder',"DeleteImage","UpdateImageDescription"),
                'users'=>array('*')
            )
        );
    }

	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionCreateGallery()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $gallery=new Galleries();
            $gallery->attributes=$_POST;
            if($gallery->save()){
                echo CJSON::encode(array("deleted"=>true));
            }else{
                echo CJSON::encode(array("deleted"=>false));
            }
        }
    }

    public function actionDeleteGallery()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $gallery=Galleries::model()->findByPk($_POST['id']);
            if($gallery->delete()){
                echo CJSON::encode(array("deleted"=>true));
            }else
                echo CJSON::encode(array("deleted"=>false,"error"=>true));
        }
    }

    public function actionRenameGallery()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $gallery=Galleries::model()->findByPk($_POST['id']);
            $gallery->attributes=$_POST;
            if($gallery->save()){
                echo CJSON::encode(array("renamed"=>true));
            }else
                echo CJSON::encode(array("renamed"=>false,"error"=>true));
        }
    }

    public function actionUploadImage(){
        $file=CUploadedFile::getInstanceByName("uploadedimage");
        //TODO CHECK IF ARLEADY EXISTS AND GENERATE NAME
        $filename=$file->name;
        if(file_exists($_SERVER['DOCUMENT_ROOT'].(Images::model()->folder).$file->name)){
            $tmp=pathinfo($file);
            $filename=$tmp["filename"]."_".time().".".$tmp["extension"];
        }
        if($file->saveAs($_SERVER['DOCUMENT_ROOT'].(Images::model()->folder).$filename)){
            $image=new Images();
            $image->filename=$filename;
            $image->galleryid=$_POST["galleryid"];
            $image->priority=$_POST["priority"];
            if(isset($_POST["description"])){
                $image->description=$_POST["description"];
            }
            $image->save();
            echo CJSON::encode(array_merge($image->attributes,array("url"=>Images::model()->folder.$image->filename)));
        }else{
            echo "ERRRRRRRORRRR";
        }

    }

    public function actionUpdateImagesOrder(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            foreach($_POST["images"] as $img){
                Yii::app()->db->createCommand()->update("images",array("priority"=>$img["priority"]),'id='.$img["id"]);
            }
            echo 1;
        }
    }

    public function actionUpdateGalleriesOrder(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            foreach($_POST["galleries"] as $g){
                Yii::app()->db->createCommand()->update("galleries",array("priority"=>$g["priority"]),'id='.$g["id"]);
            }
            echo 1;
        }
    }


    public function actionUpdateImageDescription(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $image=Images::model()->findByPk($_POST['id']);
            $image->description=$_POST["description"];
            if($image->save()){
                echo CJSON::encode(array("updated"=>true));
            }else
                echo CJSON::encode(array("updated"=>false,"error"=>true));
        }
    }

    public function deleteImageFile($filename){
        echo $filename;
        echo file_exists($filename);
        if(file_exists($filename)){
            unlink($filename);
        }
    }

    public function actionDeleteImage(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $image=Images::model()->findByPk($_POST['id']);
            if($image->delete()){
                $this->deleteImageFile($_SERVER['DOCUMENT_ROOT'].(Images::model()->folder).$image->filename);
                echo CJSON::encode(array("deleted"=>true,"file"=>$image->filename));
            }else
                echo CJSON::encode(array("deleted"=>false,"error"=>true));
        }
    }

}