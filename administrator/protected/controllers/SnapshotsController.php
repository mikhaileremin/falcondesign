<?php

class SnapshotsController extends Controller
{
    private $settings=array();

    public $defaultAction = 'portfolio';
    public function getMenu(){
        $categories=$this->convertModelToArray(Categories::model()->
            with("articles")->findAll());
        return $categories;
    }

    public function beforeAction($action){
        $settingsraw=Settings::model()->findAll();
        $settings=array();
        foreach($settingsraw as $param){
            $settings[$param["alias"]]=$param["value"];
        }
        $this->settings=$settings;
        Yii::app()->setGlobalState("settings",$settings);
        if(isset($settings["metatags"])){
            Yii::app()->setGlobalState("metatags",$settings["metatags"]);
        }
        if(isset($settings["metadescription"])){
            Yii::app()->setGlobalState("metadescription",$settings["metadescription"]);
        }

        return true;
    }

	public function actionArticles()
	{
        $settings=Settings::model()->findAll();
		$article=Articles::model()->findByPk($_GET["id"]);
        $slogan=$this->settings["slogan"];

		if(count($article)){
			echo $this->render("index",array("slogan"=>$slogan,"settings"=>$this->settings,"article"=>$article,"sidemenu"=>$this->getMenu()));
		}
		else{
			echo "Article not found";
		}
	}
	public function actionIndex(){

	}
	public function actionAbout(){
		echo $this->render("index",array("slogan"=>$this->settings["slogan"],"settings"=>$this->settings,"about"=>$this->settings["about"],"sidemenu"=>$this->getMenu()));
	}

	public function actionPortfolio(){
		$galleries=$this->convertModelToArray(Galleries::model()->
        	with("images")->findAll());

        //Make absolute urls for images
        foreach ($galleries as &$gal) {
        	foreach ($gal["images"] as &$img) {
        		$img["url"]=Images::model()->folder.$img["filename"];
        	}
        }
		echo $this->render("index",array("settings"=>$this->settings,"slogan"=>$this->settings["slogan"],"galleries"=>$galleries,"sidemenu"=>$this->getMenu()));
	}

    public function convertModelToArray($models) {
        if (is_array($models))
            $arrayMode = TRUE;
        else {
            $models = array($models);
            $arrayMode = FALSE;
        }

        $result = array();
        foreach ($models as $model) {
            $attributes = $model->getAttributes();
            $relations = array();
            foreach ($model->relations() as $key => $related) {
                if ($model->hasRelated($key)) {
                    $relations[$key] = $this->convertModelToArray($model->$key);
                }
            }
            $all = array_merge($attributes, $relations);

            if ($arrayMode)
                array_push($result, $all);
            else
                $result = $all;
        }
        return $result;
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}