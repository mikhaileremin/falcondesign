<?php

class SettingsController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('update'),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('update'),
                'users'=>array('*')
            )
        );
    }
    public function actionUpdate()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $newsettings=$_POST["settings"];
            if(!isset($newsettings["slogan"]))
                $newsettings["slogan"]="";

            if(!isset($newsettings["about"]))
                $newsettings["about"]="";
            if(isset($_POST["settings"]) && count($_POST["settings"]))
                Settings::model()->deleteAll();
            foreach($_POST["settings"] as $key=>$param){
                Yii::app()->db->createCommand()->insert("settings",array("alias"=>$key,"value"=>$param));
            }
            echo CJSON::encode(array("updated"=>true));
        }
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}