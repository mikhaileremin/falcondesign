<?php

class ApiController extends Controller
{
	
	public function actionGetData()
	{
        session_write_close();

        $data=array();
        $cr=array();
        if(isset($_GET["nocontent"])){
            $cr=array("select"=>"id,name,categoryid,modified");
        }
                    
        
        $data["articles"]=Articles::model()->findAll($cr);

        $data["categories"]=Categories::model()->findAll(array('order'=> "priority"));

        $settingsraw=Settings::model()->findAll();

        foreach($settingsraw as $param){
            $data["settings"][$param["alias"]]=$param["value"];
        }

        $data["ads"]=array();

        $data["galleries"]=$this->convertModelToArray(Galleries::model()->
        	with("images")->findAll(array('order'=> "images.priority")));

        //Make absolute urls for images
        foreach ($data["galleries"] as &$gal) {
        	foreach ($gal["images"] as &$img) {
        		$img["url"]=Images::model()->folder.$img["filename"];
        	}
        }
        $data["falconbanners"]=$this->getFalconBannerUrls();
        $data["icons"]=$this->getFalconIconsUrls();

//        $about=About::model()->find();
//        if($about){
//            $data["about"]=$about->text;
//        }


        header('Content-type: application/json');
        echo CJSON::encode($data);
	}

    public function actionGetArticles()
    {
        session_write_close();

        $data=array();
       
        $data["articles"]=Articles::model()->findAll();

        header('Content-type: application/json');
        echo CJSON::encode($data);
    }


    public function convertModelToArray($models) {
        if (is_array($models))
            $arrayMode = TRUE;
        else {
            $models = array($models);
            $arrayMode = FALSE;
        }

        $result = array();
        foreach ($models as $model) {
            $attributes = $model->getAttributes();
            $relations = array();
            foreach ($model->relations() as $key => $related) {
                if ($model->hasRelated($key)) {
                    $relations[$key] = $this->convertModelToArray($model->$key);
                }
            }
            $all = array_merge($attributes, $relations);

            if ($arrayMode)
                array_push($result, $all);
            else
                $result = $all;
        }
        return $result;
    }


    public $falconbannersfolder='/uploaded/falconbanners/';
    public $extensions=array("jpg","png","gif","jpeg");
    public function getFalconBannerUrls()
    {
        $urls=array();
        $pictures = glob($_SERVER['DOCUMENT_ROOT'].$this->falconbannersfolder."*.{gif,jpg,png,jpeg}",GLOB_BRACE);
        foreach($pictures as $p){
            $urls[]=$this->falconbannersfolder.basename($p);
        }
        return $urls;
    }

    public $falconiconsfolder='/uploaded/icons/';
    public function getFalconIconsUrls()
    {
        $urls=array();
        $pictures = glob($_SERVER['DOCUMENT_ROOT'].$this->falconiconsfolder."*.{gif,jpg,png,jpeg}",GLOB_BRACE);
        foreach($pictures as $p){
            $urls[]=$this->falconiconsfolder.basename($p);
        }
        return $urls;
    }
}