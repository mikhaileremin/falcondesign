<?php

class PasswordController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array("changePass"),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array("changePass"),
                'users'=>array('*')
            )
        );
    }
//    public function actionHashit(){
//        echo md5($_GET["p"]);
//    }
	public function actionChangePass()
	{
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            if(isset($_POST["newpass"]) && isset($_POST["newpassrepeat"]) && $_POST["newpass"] == $_POST["newpassrepeat"]){
                $currentuser=Falconusers::model()->findByPk(Yii::app()->user->id);
                $currentuser->password=md5($_POST["newpass"]);
                if($currentuser->save()){
                    echo CJSON::encode(array("changed"=>"true"));
                }else{
                    throw new CHttpException(500,"SERVER_ERROR");
                }
            }else{
                throw new CHttpException(500,"SERVER_ERROR");
            }
        }

	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}