<?php

class ArticleController extends Controller
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('deny',
                'actions'=>array('delete','save','createCategory','renameCategory','setCategoryIcon','deleteCategory',"UpdateCategoriesOrder","SetAbout"),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('delete',"save",'createCategory','renameCategory','setCategoryIcon','deleteCategory',"UpdateCategoriesOrder","SetAbout"),
                'users'=>array('*')
            )
        );
    }

	public function actionSave()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            if(!isset($_POST["id"])){
                $article=new Articles();
                $article->attributes=$_POST;
                $article->save();
                echo json_encode(array_merge($article->attributes,array("isNew"=>true)));
            }else{
                $article=Articles::model()->findByPk($_POST['id']);
                $article->attributes=$_POST;
                if($article->save())
                    echo CJSON::encode($article);
                else
                    echo "ERROR";
            }
        }else{
            echo "Forbidden";
        }

	}

	

    public function actionDelete(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $article=Articles::model()->findByPk($_POST['id']);
            if($article->delete()){
                echo CJSON::encode(array("deleted"=>true));
            }else
                echo CJSON::encode(array("deleted"=>false,"error"=>true));
        }
    }

    public function actionCreateCategory()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $category=new Categories();
            $category->attributes=$_POST;
            if($category->save()){
                echo CJSON::encode(array("deleted"=>true));
            }else{
                echo CJSON::encode(array("deleted"=>false));
            }
        }
    }

    public function actionDeleteCategory()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $category=Categories::model()->findByPk($_POST['id']);
            if($category->delete()){
                echo CJSON::encode(array("deleted"=>true));
            }else
                echo CJSON::encode(array("deleted"=>false,"error"=>true));
        }
    }

    public function actionRenameCategory()
    {
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $category=Categories::model()->findByPk($_POST['id']);
            $category->attributes=$_POST;
            if($category->save()){
                echo CJSON::encode(array("renamed"=>true));
            }else
                echo CJSON::encode(array("renamed"=>false,"error"=>true));
        }
    }

    public function actionUpdateCategoriesOrder(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            foreach($_POST["categories"] as $c){
                Yii::app()->db->createCommand()->update("categories",array("priority"=>$c["priority"]),'id='.$c["id"]);
            }
            echo 1;
        }
    }

    public function actionSetAbout(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $about=About::model()->find();
            if(!$about){
                $about=new About();
                $about->text=$_POST["about"];
            }
            $about->text=$_POST["about"];
            if($about->save()){
                echo CJSON::encode(array("saved"=>true));
            }else{
                throw new CHttpException("500","Cannot save it");
            }
        }
    }

    public function actionSetCategoryIcon(){
        if(Yii::app()->request->isAjaxRequest){
            header('Content-type: application/json');
            $category=Categories::model()->findByPk($_POST['id']);
            $category->icon=$_POST['icon'];
            if($category->save()){
                echo CJSON::encode(array("saved"=>true));
            }else{
                throw new CHttpException("500","Cannot save it");
            }
        }
    }
}