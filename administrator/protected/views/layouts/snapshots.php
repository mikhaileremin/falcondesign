<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Falcon Design</title>
	<meta name="description" content="<?php echo  Yii::app()->getGlobalState("metadescription"); ?>">
	<meta name="fragment" content="!">
    <meta name="keywords" content="<?php echo  Yii::app()->getGlobalState("metatags"); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0"/>
    <link href="/styles/snapshots.css" type="text/css" rel="stylesheet">
	<link href="/libs/bootstrap/less/bootstrap.css" type="text/css" rel="stylesheet">
    <link href="/styles/img/fav.png" rel="shortcut icon" />
</head>
<body class="falcon">

<div class="falcon-front container ">
    <?php echo $content; ?>
</div>

</body>
</html>