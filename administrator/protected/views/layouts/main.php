<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Falcon Design</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0"/>
	<link href="/libs/bootstrap/less/bootstrap.css" type="text/css" rel="stylesheet">
    <link href="/styles/img/fav.png" rel="shortcut icon" />
<!--	<link href="/styles/admin.css" type="text/css" rel="stylesheet">-->
</head>
<body>

<?php echo $content;?>
</body>
</html>
