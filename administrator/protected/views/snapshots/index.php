<?php
$this->layout = 'snapshots';
?>

<div class="row">
    <div class="col-md-3">
        <a href="/#!/" class="darklogo ">
            <img class="logo" src="/styles/img/logo.png">
        </a>
        <div class="menu">
            <a class="ng-binding" href="/#!/portfolio"><h3>Портфолио</h3></a>
            <?php
            if(isset($sidemenu)){
                foreach($sidemenu as $item){
                    ?>
                    <div class="category">
                        <h3><?php echo $item["name"]; ?></h3>
                        <ul class="nav nav-pills nav-stacked">
                            <?php foreach($item["articles"] as $a){
                              ?>
                                <li><a href="/#!/articles/<?php echo $a["id"];?>"><?php echo $a["name"];?></a></li>
                              <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <?php
                }
            }

            ?>
        </div>
    </div>
    <div class="col-md-9">
        <div class="content">
            <div class="slogan">
                <?php
                if(isset($slogan)){ echo $slogan; };
                ?>
            </div>
            <?php
            if(isset($article)){ ?>
                <h3><?php
                    echo $article["name"];
                    ?>
                </h3>
                <div>
                    <?php echo $article["content"]; ?>
                </div>
            <?php } ?>

            <?php
            if(isset($about)){ ?>
                <h3>О нас</h3>
                <div>
                    <?php echo $about; ?>
                </div>
            <?php } ?>

            <?php
            if(isset($galleries)){ ?>
                <h3><?php echo $settings["about"]; ?></h3>
                <h3><?php echo $settings["portfoliotitle"]; ?></h3>
                <div class="galleries">
                    <?php foreach ($galleries as $gal) {
                        if(count($gal["images"])){
                            ?>
                            <h4><?php echo $gal["name"]; ?></h4>
                            <div class="gallery clearfix">
                                <?php
                                foreach ($gal["images"] as $img) {
                                    ?>
                                    <div class="portfolio-image pull-left">
                                        <img class='img-thumbnail' src="<?php echo $img['url'];?>" title="<?php echo htmlentities($img['description']);?>"/>
                                        <span style="opacity: 0;height: 1px;"><?php echo $img['description'];?></span>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
