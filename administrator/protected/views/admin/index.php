<div ng-hide="applicationReady" class="mainloader" style="position:fixed;top: 50%;width:100%;color: #5E5D5D;font:10pt/1.3em Arial,Helvetica,FreeSans,sans-serif;">
	<div style="background-color:white;padding:10px;border:1px solid #EEE;margin:0 auto;width:300px;text-align:center;">
		Загрузка...
	</div>
</div>

<div ng-show="applicationReady">
	<div class="falcon-admin"></div>
</div>
<script data-main="/admin-boot.js" src="../libs/require.js"></script>