<link href="/styles/admin.css" type="text/css" rel="stylesheet">
<div class="container">
	<div class="row">
		<div class="col-md-3 col-md-offset-4 loginform_container">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'login-form',
					'htmlOptions'=>array('class'=>'form-horizontal','role'=>'form'),
					'enableClientValidation'=>true,
					'clientOptions'=>array(
						'validateOnSubmit'=>true,
					),
				)); ?>
			<div class="form-group">
				<?php echo $form->error($model,'password',array('class' => "alert alert-danger", )); ?>
			</div>    
			
			  <div class="form-group">
						<?php echo $form->label($model,'username', array('class' => 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->textField($model,'username', array('class' => 'form-control')); ?>
						</div>
					</div>		

					<div class="form-group">
						<?php echo $form->label($model,'password',array('class' => 'col-sm-2 control-label')); ?>
						<div class="col-sm-10">
							<?php echo $form->passwordField($model,'password', array('class' => 'form-control')); ?>
						</div>
					</div>


					<div class="form-group rememberMe">
						<div class="col-sm-offset-2 col-sm-10">
					      <div class="checkbox">
					        <label>
					        	<?php echo $form->checkBox($model,'rememberMe'); ?> Запомнить меня
					        </label>
					      </div>
					    </div>
					</div>

					<div class="form-group buttons">
						<div class="col-sm-offset-2 col-sm-10">
						<?php echo CHtml::submitButton('Вход', array('class' => 'btn btn-default', )); ?>
						</div>
					</div>

				<?php $this->endWidget(); ?>
		</div>
	</div>
</div>



