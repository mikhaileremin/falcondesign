define("config",[], function(){
	define('jquery', ['jQuery'], function($){ return $ });

	return require.config({
		baseUrl: '/',
		paths: {
			libs: "libs",
			app: "components/apps/app",
			adminapp:"components/apps/adminapp",
			admin: "components/apps/admin/admin",
			front: "components/apps/front/front",
			widgets: "components/widgets",
			services: "components/services",
			directives:"components/directives",
			angular: 'libs/angular/angular.min',
			"angular-ui-utils":"libs/angularUI/ui-utils.min",
            "angular-ui-sortable":"libs/angularUI/sortable",
			"angular-ui-tinymce":"libs/angularUI/tinymce",
			"tinymce":"libs/tinymce/js/tinymce.min",
			jQuery: 'libs/jquery/jquery-2.0.3.min',
            "jquery-ui-sortable":'libs/jquery/ui/minified/jquery.ui.sortable.min',
            "jquery-ui-sortable-touch":"libs/jquery/jquery.ui.touch-punch.min",
			'bootstrap':'libs/bootstrap/dist/js/bootstrap.min',
			'angular-sanitize': 'libs/angular/angular-sanitize.min',
            'angular-touch': 'libs/angular/angular-touch.min',
			'angular-animate': 'libs/angular/angular-animate.min',
			'angular-route': 'libs/angular/angular-route.min',
			'blueimp-gallery-fullscreen':'libs/Gallery/js/blueimp-gallery-fullscreen',
			'blueimp-helper':'libs/Gallery/js/blueimp-helper',
			'blueimp-gallery':'libs/Gallery/js/blueimp-gallery',
			'blueimp-gallery-indicator':'libs/Gallery/js/blueimp-gallery-indicator',
			"angular-file-upload":"libs/angular-file-upload.min"
		},

		shim: {
			'angular-sanitize': { deps: ['angular']},
			'angular-animate': { deps: ['angular']},
			'angular-route': { deps: ['angular']},
            'angular-touch': { deps: ['angular']},
			"angular-file-upload": { deps: ['angular']},

			'jQuery': { exports: '$'},

			'angular-ui-utils': { deps: ['jQuery', 'angular']},
            "libs/jquery/ui/minified/jquery.ui.core.min": { deps: ['jQuery']},
            "libs/jquery/ui/minified/jquery.ui.widget.min": { deps: ['jQuery',"libs/jquery/ui/minified/jquery.ui.core.min"]},
            "libs/jquery/ui/minified/jquery.ui.mouse.min":{ deps: ['jQuery',"libs/jquery/ui/minified/jquery.ui.widget.min"]},
            "jquery-ui-sortable-touch":{deps:["libs/jquery/ui/minified/jquery.ui.widget.min","libs/jquery/ui/minified/jquery.ui.mouse.min"]},
            "jquery-ui-sortable":{ deps: ['jQuery',"libs/jquery/ui/minified/jquery.ui.core.min","libs/jquery/ui/minified/jquery.ui.widget.min","libs/jquery/ui/minified/jquery.ui.mouse.min"]},
            "angular-ui-sortable": { deps: ['jQuery', "jquery-ui-sortable",'angular']},
			"tinymce":{ deps: ['jQuery']},
			"angular-ui-tinymce": { deps: ['jQuery', 'angular','tinymce']},
			'angular': {deps: ['jQuery'], exports: 'angular'},
			'bootstrap': {deps: ['jQuery']},
			"libs/fancybox3/jquery.fancybox":{deps:["jQuery"]},
			"libs/fancybox3/jquery.fancybox-thumbs":{deps:["jQuery","libs/fancybox3/jquery.fancybox"]},
            "libs/carousel/jquery.carouFredSel-6.2.1-packed": {deps: ['jQuery']},
            "libs/carousel/helper-plugins/jquery.touchSwipe.min": {deps: ['jQuery']}
		},
		waitSeconds: 120
	});
})