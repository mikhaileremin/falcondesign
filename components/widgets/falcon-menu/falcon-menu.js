define([
	'app',
	'css!styles/falcon.css'
], function (app) {
	"use strict";
	var directive = function ($location) {
		return {
			restrict: "C",
			scope:true,
			templateUrl: 'components/widgets/falcon-menu/falcon-menu.html',
			link: function (scope, elm, attrs) {
				scope.menuItems=[
					{
						label:"Портфолио",
						url:"#!/portfolio"
					},
//					{
//						label:"Статьи",
//						url:"#/articles"
//					},
					{
						label:"О нас",
						url:"#!/about"
					}
				]
				scope.isActive=function(url){
					if(url.substr(2).length==1){
						return $location.path()==url.substr(1);
					}else{
						return $location.path().substr(0,url.substr(2).length)==url.substr(2);
					}
				}
			}
		}
	}
	directive.$inject = ["$location"];
	app.directive('falconMenu', directive)
});