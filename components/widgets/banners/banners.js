define([
	'app',
	'components/services/falconData'
], function (app) {
	"use strict";
	var directive = function (falconData) {
		return {
			restrict: "C",
			templateUrl: 'components/widgets/banners/banners.html',
			link: function (scope, elm, attrs) {
				scope.data=falconData;
                var getRandom=function(){
                    var random_index=Math.round(Math.random()*(scope.data.falconbanners.length-1));
                    return scope.data.falconbanners[random_index];
                }
                scope.banner=getRandom();
			}
		}
	}
	directive.$inject = ["falconData"];
	app.directive('falconBanners', directive)
});