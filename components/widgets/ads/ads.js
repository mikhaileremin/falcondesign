define([
    'app',
    'components/services/falconData'
], function (app) {
    "use strict";
    var directive = function (falconData) {
        return {
            restrict: "C",
            templateUrl: 'components/widgets/ads/ads.html',
            link: function (scope) {
                scope.data=falconData;
            }
        }
    }
    directive.$inject = ["falconData"];
    app.directive('falconAds', directive)
});