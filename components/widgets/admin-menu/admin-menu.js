define([
	'adminapp'
], function (app) {
	"use strict";
	var directive = function ($location,$rootScope) {
		return {
			restrict: "C",
            replace:true,
			templateUrl: '/components/widgets/admin-menu/admin-menu.html',
			link: function (scope, elm, attrs) {
                $rootScope.appdata={
                    articleFilter:""
                }

				scope.menuItems=[
					{
						label:"Портфолио",
						url:"#/",
                        right:false
					},
					{
						label:"Статьи",
						url:"#/articles",
                        right:false
					},
//                    {
//                        label:"О нас",
//                        url:"#/about",
//                        right:false
//                    },
                    {
                        label:"Настройки",
                        url:"#/settings",
                        right:false
                    },
					{
						label:"Сменить пароль",
						right:true,
						url:"#/passchange"
					},
                    {
                        label:"Выход",
                        right:true,
                        url:"admin/logout"
                    }
				]
				scope.isActive=function(url){
					if(url.substr(1).length==1){
						return $location.path()==url.substr(1);
					}else{
						return $location.path().substr(0,url.substr(1).length)==url.substr(1);
					}
				}

                scope.isArticles=function(){
                    if(~$location.path().indexOf("articles")){
                        return true;
                    }else
                        return false;
                }


			}
		}
	}
	directive.$inject = ["$location","$rootScope"];
	app.directive('adminMenu', directive)
});