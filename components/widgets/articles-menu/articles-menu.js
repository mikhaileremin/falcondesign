define([
	'app',
	'components/services/falconData',
	'css!styles/falcon.css'
], function (app) {
	"use strict";
	var directive = function (falconData,$routeParams,$location) {
		return {
			restrict: "C",
			scope:true,
			templateUrl: 'components/widgets/articles-menu/articles-menu.html',
			link: function (scope, elm, attrs) {
				scope.data=falconData;
                scope.openCategory=falconData.article.categoryid;

				scope.isActive=function(id,portfolio){
                    if(portfolio)
                        return ~$location.path().indexOf("portfolio");
					return id==$routeParams.articleId;
				}

                scope.isActiveCategory=function(cid){
                    return $routeParams.articleId && falconData.article.categoryid==cid;
                }
                scope.showCategory=function(id){
                    scope.openCategory=id;
                }
                scope.isOpen=function(id){
                    return id==scope.openCategory;
                }
			}
		}
	}
	directive.$inject = ["falconData","$routeParams","$location"];
	app.directive('falconArticlesMenu', directive)
});