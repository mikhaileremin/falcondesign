define([
	'app',
	"libs/fancybox3/jquery.fancybox",
	"libs/fancybox3/jquery.fancybox-thumbs",
	"css!libs/fancybox3/jquery.fancybox.css",
	"css!libs/fancybox3/jquery.fancybox-thumbs.css",

	'libs/carousel/jquery.carouFredSel-6.2.1-packed',
	'libs/carousel/helper-plugins/jquery.touchSwipe.min',
	'css!components/widgets/gallery/gallery.css',
	'css!libs/Gallery/css/blueimp-gallery.min.css',
	'css!libs/Gallery/css/blueimp-gallery-indicator.css'
], function (app,blueimp) {
	"use strict";
	var directive = function (falconData) {
		return {
			restrict: "C",
			scope:{
				images:"=",
                name:"@"
			},
			templateUrl: 'components/widgets/gallery/gallery.html',
			link: function (scope, elm, attrs) {
                scope.$watch("images",function(newimageset){
                    setTimeout(function(){
                        elm.find(".carousel").carouFredSel({
                            responsive:true,
                            scroll:2,
                            auto:false,
                            width:"100%",
                            height:120,
                            items:{
                                visible:5,
                                width:200 //doesn't matter but component asks for it :) here you are mister component
                            },
                            swipe:{
                                onTouch:true,
                                onMouse:true
                            },
                            prev	: {
                                button	: elm.find(".prev"),
                                key		: "left"
                            },
                            next	: {
                                button	: elm.find(".next"),
                                key		: "right"
                            }
                        });
                        elm.find("[rel='fancybox-thumb']").fancybox({
                            padding:0,
                            live:false,
                            helpers : {
                                thumbs : true
                            }
                        });
                    },0)
                })

                scope.$on("$destroy",function(){
                    //destroy bindings on element;
                })
			}
		}
	}
	directive.$inject = ["falconData"];
	app.directive('imgGallery', directive)
});