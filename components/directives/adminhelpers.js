define([
	'adminapp',
	'jquery'
], function (app,$) {
	"use strict";
	//CLICK OUTSIDE ELEMENT
	var outside=function($document){
		return {
			restrict: 'A',
			link: function(scope, elem, attr, ctrl) {
				var handler = function(e) {
					var target = $( e.target );


					if ( !elem.has( target ).length && elem !== target ) {

						//check for exceptions that should work as not outside clicks
						if(attr.exceptOf){
							var selectors=attr.exceptOf.split(",");
							if(!selectors.length){
								selectors=[attr.exceptOf]
							}
							var match=false;
							angular.forEach(selectors,function(s){
								s= $.trim(s);
								if(target.is(s) || target.has(s).length || target.parents(s).length )
									match=true;
							});
							if(match)
								return;
						}

						scope.$apply(attr.clickOutside);
					}
				};
				$document.bind('click', handler);
				scope.$on('$destroy', function() {
					$document.unbind('click', handler);
				});
			}
		}
	}

	outside.$inject = ['$document'];
	app.directive("clickOutside", outside);

    app.directive('falconCollapse', function() {
        return function(scope, elm, attrs) {
            $(elm).nextAll(attrs.toggle).collapse({
                toggle: false
            })
            $(elm).click(function(){
                $(elm).parents(attrs.parent).find(attrs.toggle).collapse("hide")
                $(elm).nextAll(attrs.toggle).collapse("toggle")
            })
//            scope.$on('$destroy', function() {
//                $(elm).remove();
//            });
        };
    });
});
