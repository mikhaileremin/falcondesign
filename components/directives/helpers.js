define([
	'app',
	'jquery',
    "bootstrap"
], function (app,$) {
	"use strict";
	app.directive('scrollToTop', function() {
		return function(scope, elm, attrs) {
			$(elm).click(function(){
				$('body,html').animate({
					scrollTop:0
				},500);
			})
		};
	});

    app.directive('falconCollapse', function() {
        return function(scope, elm, attrs) {
            $(elm).next(attrs.toggle).collapse({
                toggle: false
            }).addClass("falcon-collapse");
            $(elm).click(function(){
                $(elm).parents(attrs.parent).find(attrs.toggle).collapse("hide")
                $(elm).next(attrs.toggle).collapse("toggle")
            })
//            scope.$on('$destroy', function() {
//                $(elm).remove();
//            });
        };
    });


});