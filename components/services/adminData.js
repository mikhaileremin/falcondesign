define([
    "adminapp",
    "angular",
    "jquery"
], function(app,angular,$){
    "use strict";
    var service = function($http,$rootScope){

        var _public = {
            loading: true,
            article:false,
            settings:{}
        };
        _public.refreshData=function(loadarticles){
            return $http({
                url: "/administrator/api/GetData"+(loadarticles?'':'?nocontent=1'),
                method: "GET"
            }).success(function(data){
                _public.loading=false;
                $rootScope.applicationReady = true;
                if(!loadarticles){
                    angular.forEach(data.articles,function(ar){
                        delete ar.content;
                    })
                }
                $.extend(true,_public,data);
            });
        }
        // get data on init
        _public.refreshData(true);

        _public.saveArticle=function(article){
            // get data on init
            return $.ajax({
                url: "/administrator/article/save",
                data:article,
                dataType:"json",
                type: "POST"
            })
        }
        _public.deleteArticle=function(id){
            return $.ajax({
                url: "/administrator/article/delete",
                data:{id:id},
                dataType:"json",
                type: "POST"
            })
        }

	    _public.createCategory=function(name){
		    return $.ajax({
			    url: "/administrator/article/createCategory",
			    data:{name:name},
			    dataType:"json",
			    type: "POST"
		    })
	    }

	    _public.deleteCategory=function(id){
		    return $.ajax({
			    url: "/administrator/article/deleteCategory",
			    data:{id:id},
			    dataType:"json",
			    type: "POST"
		    })
	    }

	    _public.renameCategory=function(id,name){
		    return $.ajax({
			    url: "/administrator/article/renameCategory",
			    data:{id:id,name:name},
			    dataType:"json",
			    type: "POST"
		    })
	    }

        _public.setCategoryIcon=function(id,icon){
            return $.ajax({
                url: "/administrator/article/setCategoryIcon",
                data:{id:id,icon:icon},
                dataType:"json",
                type: "POST"
            })
        }

        //GALLERIES

        _public.createGallery=function(name){
            return $.ajax({
                url: "/administrator/portfolio/createGallery",
                data:{name:name},
                dataType:"json",
                type: "POST"
            })
        }

        _public.deleteGallery=function(id){
            return $.ajax({
                url: "/administrator/portfolio/deleteGallery",
                data:{id:id},
                dataType:"json",
                type: "POST"
            })
        }

        _public.renameGallery=function(id,name){
            return $.ajax({
                url: "/administrator/portfolio/renameGallery",
                data:{id:id,name:name},
                dataType:"json",
                type: "POST"
            })
        }

	    _public.updateImagesOrder=function(images){
            var tosend=[]
            angular.forEach(images,function(img){
                tosend.push({id:img.id,priority:img.priority});
            })
		    return $.ajax({
			    url: "/administrator/portfolio/UpdateImagesOrder",
			    data:{images:tosend},
			    dataType:"json",
			    type: "POST"
		    })
	    }

        _public.updateGalleriesOrder=function(galleries){
            var tosend=[]
            angular.forEach(galleries,function(g){
                tosend.push({id:g.id,priority:g.priority});
            })
            return $.ajax({
                url: "/administrator/portfolio/UpdateGalleriesOrder",
                data:{galleries:tosend},
                dataType:"json",
                type: "POST"
            })
        }

        _public.updateCategoriesOrder=function(categories){
            var tosend=[]
            angular.forEach(categories,function(g){
                tosend.push({id:g.id,priority:g.priority});
            })
            return $.ajax({
                url: "/administrator/article/UpdateCategoriesOrder",
                data:{categories:tosend},
                dataType:"json",
                type: "POST"
            })
        }





	    _public.deleteImage=function(id){
		    return $.ajax({
			    url: "/administrator/portfolio/deleteImage",
			    data:{id:id},
			    dataType:"json",
			    type: "POST"
		    })
	    }

        _public.updateImageDescription=function(image){
            return $.ajax({
                url: "/administrator/portfolio/updateImageDescription",
                data:{id:image.id,description:image.description},
                dataType:"json",
                type: "POST"
            })
        }

        _public.updateSettings=function(settings){
            return $.ajax({
                url: "/administrator/settings/update",
                data:{settings:settings},
                dataType:"json",
                type: "POST"
            })
        }

        _public.updateAbout=function(about){
            return $.ajax({
                url: "/administrator/article/setAbout",
                data:{about:about},
                dataType:"json",
                type: "POST"
            })
        }

        _public.changePassword=function(newpass,newpassrepeat){
            return $.ajax({
                url: "/administrator/password/changePass",
                data:{newpass:newpass,newpassrepeat:newpassrepeat},
                dataType:"json",
                type: "POST"
            })
        }





        return _public;
    };
    service.$inject = [ "$http","$rootScope","$routeParams"];
    app.factory("adminData", service);
    return service;
});