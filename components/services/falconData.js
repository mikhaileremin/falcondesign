define([
	"app",
	"angular"
], function(app,angular){
	"use strict";
	var service = function($http,$rootScope,$routeParams,$timeout){

		var _public = {
			loading: true,
            articlesloaded:false,
			article:false
		};

		// get data on init
		$http({
			url: "/administrator/api/GetData?nocontent=1",
//			url: "/testdata/data.json",
			method: "GET"
		}).success(function(data){
			_public.loading=false;
            if($routeParams.articleId){
                _public.loadArticle($routeParams.articleId);
            }

            $rootScope.applicationReady = true;
			angular.extend(_public,data);
				// get data on init
				$http({
					url: "/administrator/api/GetArticles",
					method: "GET"
				}).success(function(data){
						angular.extend(_public,data);
                        _public.articlesloaded=true;
						if($routeParams.articleId){
							_public.loadArticle($routeParams.articleId);
						}

					});
		});

		_public.loadArticle=function(id){
            if(_public.loading || !_public.articles){
                $timeout(function(){
                    _public.loadArticle(id);
                },200)
            }else{
                var loadedArticle=false;
                angular.forEach(_public.articles,function(article){
                    if(article.id==id){
                        loadedArticle=article;
                        _public.article=article;
                    }

                })
                return loadedArticle;
            }

		}
        $rootScope.$watch(function(){
            return $routeParams.articleId;
        },function(id){
            _public.loadArticle(id);
        })
		return _public;
	};
	service.$inject = [ "$http","$rootScope","$routeParams" ,"$timeout"];
	app.factory("falconData", service);
	return service;
});