define([
	'app',
	'widgets/gallery/gallery',
    'components/services/falconData'
], function (app) {
	"use strict";
	var controller = function ($scope,falconData) {
        $scope.data=falconData;
	}
	controller.$inject = ["$scope","falconData"];
	app.controller('portfolioPageCtrl', controller)
});