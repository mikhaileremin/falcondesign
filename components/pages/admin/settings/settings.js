define([
    'adminapp',
    'components/services/adminData',
    'css!styles/admin.css'
], function (app) {
    "use strict";
    var controller = function ($scope,adminData) {
            $scope.data=adminData;
            $scope.working=false;
            $scope.modified=false;

            $scope.save=function(){
                $scope.working=true;
                adminData.updateSettings($scope.data.settings).success(function(){
                    $scope.$apply(function(){
                        $scope.working=false;
                        $scope.modified=false;
                    });
                });
            }

            $scope.cancel=function(){
                $scope.working=true;
                adminData.refreshData().success(function(){
                    $scope.working=false;
                    $scope.modified=false;
                });
            }

            $scope.setModified=function(){
                $scope.modified=true;
            }
            $scope.editorOptions={
                plugins:["image","code","preview"],
                image_advtab:true,
                language : 'ru',
                relative_urls : false,
                height:300,
                toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link unlink anchor | image | print preview code"
            }

    }
    controller.$inject = ["$scope","adminData"];
    app.controller('adminSettingsPageCtrl', controller)
});