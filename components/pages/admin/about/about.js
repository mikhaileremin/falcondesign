define([
    'adminapp',
    'components/services/adminData',
    'css!styles/admin.css'
], function (app) {
    "use strict";
    var controller = function ($scope,adminData) {
        $scope.data=adminData;
        $scope.working=false;
        $scope.save=function(){
            $scope.working=true;
            adminData.updateAbout($scope.data.about).done(function(){
                $scope.$apply(function(){$scope.working=false});
            }).fail(function(){
                $scope.$apply(function(){$scope.working=false});
            });
        }

        $scope.cancel=function(){
            adminData.refreshData();
        }
        $scope.editorOptions={
            plugins:["image","code","preview"],
            image_advtab:true,
            language : 'ru',
            relative_urls : false,
            height:400,
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link unlink anchor | image | print preview code"
        }

    }
    controller.$inject = ["$scope","adminData"];
    app.controller('adminAboutPageCtrl', controller)
});