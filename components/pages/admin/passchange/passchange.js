define([
    'adminapp',
    'components/services/adminData',
    'css!styles/admin.css'
], function (app) {
    "use strict";
    var controller = function ($scope,adminData,$timeout) {
        $scope.error=false;
        $scope.changed=false;
        $scope.newpassword="";
        $scope.newpasswordrepeat="";
        $scope.working=false;
        $scope.changePassword=function(){
            if($scope.newpassword==$scope.newpasswordrepeat && $scope.newpasswordrepeat.length && $scope.newpassword.length){
                   $scope.working=true;
                   adminData.changePassword($scope.newpassword,$scope.newpasswordrepeat).
                       success(function(){
                           $scope.$apply(function(){
                               $scope.working=false;
                               $scope.error=false;
                               $scope.success=true;
                               $timeout(function(){
                                   $scope.success=false;
                               },5000)
                               $scope.newpassword="";
                               $scope.newpasswordrepeat="";
                           })
                       }).
                       fail(function(){
                           $scope.$apply(function(){
                               $scope.error="Возникла серверная ошибка! Возможно вы хитрите или делаете что-то не так";
                               $scope.newpassword="";
                               $scope.newpasswordrepeat="";
                               $scope.working=false;
                           })
                       })
            }else{
                $scope.error="Введеные пароли не совпадают или не введены. Будьте внимательны!"
            }
        }
    }
    controller.$inject = ["$scope","adminData","$timeout"];
    app.controller('adminChangePassPageCtrl', controller)
});