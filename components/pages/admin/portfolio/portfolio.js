define([
    'adminapp',
    'components/services/adminData',
	"angular-file-upload"
], function (app) {
    "use strict";
    var controller = function ($scope,adminData,$fileUploader,$timeout) {
        $scope.data=adminData;
        $scope.saving=false;
        $scope.newgalleryname="";

        $scope.isActive=function(id){
            if($scope.article)
                return id==$scope.article.id;
            else
                return false;
        }

        $scope.resetGalleryName=function(){
            $timeout(function(){
                $scope.newgalleryname="";
            },500)
        }
        /**
         * CATEGORIES FUNCTIONAL
         */
        $scope.newGallery=function(){
            if($scope.newgalleryname.length){
                adminData.createGallery($scope.newgalleryname).success(function(){
                    adminData.refreshData();
                })
            }
            $scope.newgalleryname="";
        }

        $scope.deleteGallery=function(id,name){
            if(confirm("Вы уверены что хотите удалить категорию \""+name+"\"?")){
                adminData.deleteGallery(id).success(function(data){
                    adminData.refreshData();
                })
            }else{
                return;
            }

        }

	    $scope.deleteImage=function(id,index){
		    $scope.openedGallery.images.splice(index,1);
		    adminData.deleteImage(id).success(function(){
		    })
	    }

        $scope.editGallery=function(galleryid){
            $scope.editgallery=galleryid;
            setTimeout(function(){
                angular.element('.gallerynameinput'+galleryid).focus();
            },0)
        }

        $scope.changeGalleryName=function(galleryid,name){
            if(name.length){
                adminData.renameGallery(galleryid,name).success(function(){
                    adminData.refreshData();
                })
                $scope.cancelEditGallery();
            }
        }

        $scope.cancelEditGallery=function(){
            $scope.editgallery=false;
            adminData.refreshData();
        }

        $scope.openGallery=function(gallery){
            $scope.openedGallery=gallery;
        }


	    $scope.getNewOrder=function(){
            if($scope.openedGallery.images.length)
                return $scope.openedGallery.images.length;
		    else
                return 0;
	    }

	    $scope.updateImage=function(image){
//			adminData.updateImage({
//
//			});
	    }

	    //IMAGE UPLOAD
	    $scope.uploader = $fileUploader.create({
		    scope: $scope,   // to automatically update the html. Default: $rootScope
		    autoUpload: true,
            removeAfterUpload:true,
            alias:"uploadedimage",
            url: '/administrator/portfolio/UploadImage',
		    filters: [
			    function (item) {
				    return (~item.type.indexOf("jpeg") || ~item.type.indexOf("gif") || ~item.type.indexOf("png"))
			    }
		    ]
	    }).bind("beforeupload",function(event,item){
		    item.formData=[
			    {
				    galleryid:$scope.openedGallery?$scope.openedGallery.id:1,
				    priority:$scope.getNewOrder()
			    }
		    ];
		}).bind("complete",function(event, xhr, item, response){
            $scope.openedGallery.images.push(response);
        });

        $scope.saveDescription=function(image){
            image.edit=false;
            adminData.updateImageDescription(image);
        }


        $scope.remapPositions=function(){
            angular.forEach($scope.openedGallery.images,function(img,i){
                img.priority=i;
            })
        }

	    $scope.sortableOpts={
		    "update":function(event,ui){
			    $timeout(function(){
                    //priority - was, index - now
                    var slice_from=(ui.item.scope().image.priority>ui.item.scope().$index)?
                            ui.item.scope().$index:
                            ui.item.scope().image.priority,
                        slice_to=(ui.item.scope().image.priority>ui.item.scope().$index)?
                            ui.item.scope().image.priority:
                            ui.item.scope().$index;


                    $scope.remapPositions();
                    //right-to left
                    $scope.openedGallery.images.slice(slice_from,slice_to);
					adminData.updateImagesOrder(
                            $scope.openedGallery.images.slice(ui.item.scope().$index,
                            $scope.openedGallery.images.length)
                        ).success(function(){

                    })
			    },0)

		    }
	    }

        $scope.gallerySortableOpts={
            "update":function(event,ui){
                var remapGalPriors=function(){
                    angular.forEach($scope.data.galleries,function(g,i){
                        g.priority=i;
                    })
                }
                $timeout(function(){
                    remapGalPriors();
                    adminData.updateGalleriesOrder($scope.data.galleries).success(function(){
                    })
                },0)

            }
        }

    }
    controller.$inject = ["$scope","adminData","$fileUploader","$timeout"];
    app.controller('adminPortfolioCtrl', controller)
});