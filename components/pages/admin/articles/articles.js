define([
    'adminapp',
    'directives/adminhelpers',
    'components/services/adminData'
], function (app) {
    "use strict";
    var controller = function ($scope,adminData,$timeout,$rootScope) {
        $scope.data=adminData;
        $scope.saving=false;
        $scope.modified=false;
        $scope.newcategoryname="";

        $scope.editorOptions={
            plugins:["image","code","preview"],
            image_advtab:true,
            language : 'ru',
            height:250,
            relative_urls : false,
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor backcolor | link unlink anchor | image | print preview code"
        }
        var ajaxErrorHandler=function(xhr){
            $scope.error="Странное дело! Возникла ошибка. Попробуйте еще раз или позже";
            $scope.saving=false;
            $timeout(function(){
                $scope.error=false;
            },5000)
        }

        $scope.editArticle=function(article){
            $scope.article=article;
            $('body,html').animate({
                scrollTop:0
            },500);
        }


        $scope.filterArticles=function(item){
            if(~item.name.indexOf($rootScope.appdata.articleFilter))
                return true;
            else
                return false;
        }
        $scope.resetCategoryName=function(){
            $timeout(function(){
                $scope.newcategoryname="";
            },500)
        }
        $scope.isActive=function(id){
            if($scope.article)
                return id==$scope.article.id;
            else
                return false;
        }

        $scope.newArticle=function(categoryid){
            if($scope.modified){
                if(confirm("Несохраненные изменения в статье будут отмены, вы уверены?")){
                    $scope.modified=false;
                    $scope.article={};
                    $scope.article.categoryid=categoryid||$scope.data.categories[0].id;
                }else{
                    return;
                }
            }else{
                $scope.article={};
                $scope.article.categoryid=$scope.data.categories[0].id;
            }
        }

        $scope.saveArticle=function(){
            $scope.saving=true;
            adminData.saveArticle($scope.article).success(function(data){

                $scope.$apply(function(){
                    $scope.modified=false;

                    angular.forEach($scope.data.articles,function(a){
                        $scope.article.modified=new Date().getTime();
                        if(a.id==$scope.article.id){
                            a.modified=new Date().getTime();
                        }
                        console.log(a.modified,typeof(a.modified))
                    })
                    $scope.saving=false;
                })
                if(data.isNew)
                    adminData.refreshData();
            }).fail(ajaxErrorHandler)
        }

        $scope.deleteArticle=function(article){
            if(confirm("Вы уверены что хотите удалить статью \""+article.name+"\"?")){
                adminData.deleteArticle(article.id).success(function(){
                    $scope.article=false;
                    adminData.refreshData();
                })
            }else{
                return;
            }
        }

        $scope.setModified=function(){
            $scope.modified=true;
        }

        /**
         * CATEGORIES FUNCTIONAL
         */
        $scope.newCategory=function(){
            if($scope.newcategoryname.length){
                adminData.createCategory($scope.newcategoryname).success(function(){
                    adminData.refreshData();
                })
            }
            $scope.newcategoryname="";
        }

        $scope.deleteCategory=function(id,name){
            if(confirm("Вы уверены что хотите удалить категорию \""+name+"\"?")){
                adminData.deleteCategory(id).success(function(data){
                    adminData.refreshData();
                })
            }else{
                return;
            }

        }

        $scope.hasArticles=function(categoryid){
            var has=false;
            angular.forEach($scope.data.articles,function(article){
                if(article.categoryid==categoryid){
                    has=true;
                }
            })
            return has;
        }

        $scope.editCategory=function(categoryid){
            $scope.editcategory=categoryid;
            setTimeout(function(){
                $('.categorynameinput'+categoryid).focus();
            },0)
        }

        $scope.changeCategoryName=function(categoryid,name){
            if(name.length){
                adminData.renameCategory(categoryid,name).success(function(){
                    adminData.refreshData();
                })
                $scope.cancelEditCategory();
            }
        }

        $scope.setIcon=function(category,icon){
            category.icon=icon;
            adminData.setCategoryIcon(category,icon);
        }

        $scope.cancelEditCategory=function(){
            $scope.editcategory=false;
            adminData.refreshData();
        }

        $scope.categoriesSortableOpts={
            "start":function(){
                $scope.$apply(function(){
                    $scope.sortingcategs=true;
                })
            },
            "stop":function(){
                $scope.$apply(function(){
                    $scope.sortingcategs=false;
                })
            },
            "update":function(event,ui){
                var remapCategPriors=function(){
                    angular.forEach($scope.data.categories,function(g,i){
                        g.priority=i;
                    })
                }
                $timeout(function(){
                    remapCategPriors();
                    adminData.updateCategoriesOrder($scope.data.categories).success(function(){
                    })
                },0)

            }
        }

    }
    controller.$inject = ["$scope","adminData","$timeout","$rootScope"];
    app.controller('adminArticlesPageCtrl', controller);
});