define([
	'app',
	'components/services/falconData',
	'css!styles/falcon.css'
], function (app) {
	"use strict";
	var controller = function ($scope,$routeParams,falconData) {
		$scope.data=falconData;
	}
	controller.$inject = ["$scope","$routeParams","falconData"];
	app.controller('aboutCtrl', controller)
});