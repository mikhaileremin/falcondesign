define([
	// Standard Libs
	'angular',		// lib/angular/angular
	'angular-sanitize',
	'angular-route',
    'angular-animate',
//    'angular-touch',
	"angular-ui-utils",
    "jquery-ui-sortable",
    "jquery-ui-sortable-touch",
    "angular-ui-sortable",
	'tinymce',
	'angular-ui-tinymce'
], function (angular) {
	"use strict";
	var app = angular.module('Application', ["ngSanitize","ngRoute","ngAnimate","ui.utils","ui.tinymce","ui.sortable","angularFileUpload"]);

	app.config(function($routeProvider){
		$routeProvider.
			when('/', {
				controller:"adminPortfolioCtrl",
                templateUrl: '/components/pages/admin/portfolio/portfolio.html'
			}).
			when('/articles', {
				controller:"adminArticlesPageCtrl",
				templateUrl: '/components/pages/admin/articles/articles.html'
			}).
			when('/portfolio', {
				controller:"adminPortfolioCtrl",
				templateUrl: '/components/pages/admin/portfolio/portfolio.html'
			}).
            when('/about', {
                controller:"adminAboutPageCtrl",
                templateUrl: '/components/pages/admin/about/about.html'
            }).
            when('/settings', {
				controller:"adminSettingsPageCtrl",
				templateUrl: '/components/pages/admin/settings/settings.html'
			}).
			when('/passchange', {
				controller:"adminChangePassPageCtrl",
				templateUrl: '/components/pages/admin/passchange/passchange.html'
			});
	})
	app.run(["$rootScope", function($rootScope){
		$rootScope.applicationReady = true;
	}]);
	return app;
});