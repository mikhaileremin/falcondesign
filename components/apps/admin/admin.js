define([
	'adminapp',
	'bootstrap',
	'components/widgets/admin-menu/admin-menu',
	'components/pages/admin/articles/articles',
	'components/pages/admin/portfolio/portfolio',
	'components/pages/admin/settings/settings',
    'components/pages/admin/about/about',
	'components/pages/admin/passchange/passchange'
], function (app) {
	"use strict";
	var directive = function () {
		return {
			restrict: "C",
			templateUrl: '/components/apps/admin/admin.html',
			link: function (scope, elm, attrs) {

			}
		}
	}
	directive.$inject = [];
	app.directive('falconAdmin', directive)
});