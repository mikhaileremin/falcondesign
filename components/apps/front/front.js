define([
	'app',
	'bootstrap',
	'components/pages/portfolio/portfolio',
	'components/pages/article/article',
	'components/pages/about/about',

    "components/services/falconData",
	'components/directives/helpers',

	'components/widgets/banners/banners',
    'components/widgets/ads/ads',
	'components/widgets/articles-menu/articles-menu',
	'components/widgets/falcon-menu/falcon-menu',
	'css!styles/falcon.css'
], function (app) {
	"use strict";
	var directive = function (falconData) {
		return {
			restrict: "C",
			templateUrl: 'components/apps/front/front.html',
			link: function (scope, elm, attrs) {
                scope.data=falconData;
                scope.getCurrentYear=function(){
                    var d=new Date();
                    return d.getFullYear();
                }
                scope.$on("$viewContentLoaded",function(){
                    setTimeout(function(){
                        $('body,html').animate({
                            scrollTop:0
                        },500);
                    },100)
                })

                scope.closeMenus=function(){
                    elm.find(".falcon-collapse").collapse("hide")
                }
			}
		}
	}
	directive.$inject = ["falconData"];
	app.directive('falconFront', directive);
	return app;
});