define([
	// Standard Libs
	'angular',		// lib/angular/angular
	'angular-sanitize',
    'angular-animate',
	'angular-route',
	"angular-ui-utils"
], function (angular) {
	"use strict";
	var app = angular.module('Application', ["ngSanitize","ngRoute","ngAnimate","ui.utils"]);

	app.config(function($routeProvider,$locationProvider ){
		$routeProvider.
		when('/portfolio',{
			controller:"portfolioPageCtrl",
			templateUrl: 'components/pages/portfolio/portfolio.html'
		}).
		when('/articles',{
			templateUrl: 'components/pages/article/list.html'
		}).
		when('/articles/:articleId',{
			controller:"articleCtrl",
			templateUrl: 'components/pages/article/article.html'
		}).
		when('/about',{
			controller:"aboutCtrl",
			templateUrl: 'components/pages/about/about.html'
		}).
		otherwise({
			redirectTo: '/portfolio'
		});

		$locationProvider.hashPrefix('!');

	})
	app.run(["$rootScope", function($rootScope){

	}]);

	app.filter('unsafe', function($sce) {
		return function(val) {
			return $sce.trustAsHtml(val);
		};
	});
	return app;
});