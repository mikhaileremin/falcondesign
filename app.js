var express = require('express'),
	app = express();
var lessMiddleware = require('less-middleware');

app.use(lessMiddleware({
	src: __dirname + '/',
	dest: __dirname + "/",
	compress: false
}));

app.use('/',express.static(__dirname + "/", { maxAge: false }));


require('http').createServer(app).listen(2222);