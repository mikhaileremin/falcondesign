"use strict";
require(['config'],function(require){
	require([
		'app',
		'angular',
		'components/apps/front/front'
	], function(app,angular){
		angular.bootstrap(document, ['Application']);
	});
})