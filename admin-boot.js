"use strict";
require(['config'],function(require){
	require([
		'adminapp',
		'angular',
		'components/apps/admin/admin'
	], function(app,angular){
		angular.bootstrap(document, ['Application']);
	});
})

